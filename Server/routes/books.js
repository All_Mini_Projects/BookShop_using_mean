const express = require('express'); 
const mysql = require('mysql');    
const config = require('config');
const booksHandler = express();

const connection = mysql.createConnection({
    host:config.get("host"),
    user:config.get("user"),
    password:config.get("password"),
    database:config.get("database")
});

var bookData = null;

connection.connect();

booksHandler.get("/",(req,res)=>{
    connection.query(`select * from books`,(err,result)=>{
                    if( err == null ){
                        bookData = result;
                        res.cotentType = "application/json";
                        res.send(JSON.stringify(bookData));
                        console.log(JSON.stringify(bookData));
                    }else{
                        res.send("Something went Wrong");
                    }
    });
});


booksHandler.get("/:No",(req,res)=>{
	let boolId = req.params.No;
    connection.query(`select * from books where book_id =${boolId}`,(err,result)=>{
                    if( err == null ){
                        bookData = result;
                        res.cotentType = "application/json";
                        res.send(JSON.stringify(bookData));
                        console.log(JSON.stringify(bookData));
                    }else{
                        res.send("Something went Wrong");
                    }
    });
});

booksHandler.post("/",(req,res)=>{
    console.log("inside");
    let bookName = req.body.book_name;
    let authorName = req.body.author;
    let stock = req.body.stock;
    let price = req.body.price;
    let q = `insert into books(book_name,author,stock,price) 
    values('${bookName}','${authorName}',${stock},${price})`;
    console.log(q);
    connection.query(q,(err,result)=>{
        if( err == null ){
            res.cotentType = "application/json";
            res.send(JSON.stringify(result));
            console.log("Done");
        }else{
            res.send("Something went Wrong");
        }
    });
});


/**
 postman Input for post

 {
    "bookName":"xyz",
    "authorName":"me",
    "stock":1,
    "price":10
}

 */

booksHandler.put("/:No",(req,res)=>{
    let boolId = req.params.No;
    let price = req.body.price;
    let stock = req.body.stock;
    let q = `update books set stock=${stock},price=${price} where book_id=${boolId}`;
    console.log(q);
    connection.query(q,(err,result)=>{
        if( err == null ){
            res.cotentType = "application/json";
            res.send(JSON.stringify(result));
            console.log("Done");
        }else{
            res.send("Something went Wrong");
        }
    });
});

booksHandler.delete("/:No",(req,res)=>{
    let boolId = req.params.No;
    let q = `delete from books where book_id = ${boolId}`;
    console.log(q);
    connection.query(q,(err,result)=>{
        if( err == null ){
            res.cotentType = "application/json";
            res.send(JSON.stringify(result));
            console.log("Done");
        }else{
            res.send("Something went Wrong");
        }
    });
});

module.exports = booksHandler;
