const express = require('express');
const sellerRoutes = require('./routes/seller')
const booksRoutes = require('./routes/books')

const myApp =  express();

myApp.use((req,res,next)=>{
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Methods","GET,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");
    next();
});

myApp.use(express.json())
myApp.use("/seller",sellerRoutes);
myApp.use("/books",booksRoutes);


myApp.listen(5000,()=>{ console.log("Server running...")});
