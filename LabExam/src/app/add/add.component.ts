import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  book = {"book_name":"" ,"author":"" ,"stock":"" ,"price":"" };
  constructor(private service: DataService,  
    private currentActiveRoute: ActivatedRoute,
    private router:Router) { }

  ngOnInit(): void {
  }

  AddBook(){
    this.service.AddBook(this.book)
                  .subscribe((response)=>{
                        console.log("is");
                        this.router.navigate(['home']);    
    });
  }
}
