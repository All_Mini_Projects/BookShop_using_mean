import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-sing-in',
  templateUrl: './sing-in.component.html',
  styleUrls: ['./sing-in.component.css']
})
export class SingInComponent implements OnInit {

  seller ={"name":"","email":"","password":"","dom":"","mobileno":"","city":""};
  constructor(private service:DataService, private route: Router) { }

  ngOnInit(): void {
  }

  ResgisterSeller(){
    var sellerData:any;
    this.service.AddSeller(this.seller)
                  .subscribe((response)=>{
                    sellerData = response;
                        console.log("is");
                        this.route.navigate(['login']);    
    });
  }
}
