import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  constructor(private service: DataService,  
    private currentActiveRoute: ActivatedRoute,
    private router:Router) { }

  ngOnInit(): void {
    this.currentActiveRoute.paramMap.subscribe((result)=>{
      let book_Id = result.get("No");
      this.service.DeleteBook(book_Id).subscribe((result)=>{
        this.router.navigate(['/home']);
      });
    });
  }

}
