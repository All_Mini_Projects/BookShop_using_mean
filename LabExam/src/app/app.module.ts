import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SingInComponent } from './sing-in/sing-in.component';
import { DataService } from './data.service';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';
import { AddComponent } from './add/add.component';
import { TryComponent } from './try/try.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SingInComponent,
    HomeComponent,
   AddComponent,
   EditComponent,
    DeleteComponent,
    TryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path: "", component: LoginComponent},
      {path: "login", component: LoginComponent},
      {path: "sigin", component: SingInComponent},
      {path: "home", component: HomeComponent},
      {path: "add", component: AddComponent},
      {path: "edit/:No", component: EditComponent},
      {path: "delete/:No", component: DeleteComponent}
      
    ])
  ],
  providers: [HttpClient, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
