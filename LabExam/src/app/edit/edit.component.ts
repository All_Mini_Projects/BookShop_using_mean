import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  
  book:any;
  constructor(private service: DataService,  
    private currentActiveRoute: ActivatedRoute,
    private router:Router) { }

  ngOnInit(): void {
    var bookData:any;
    this.currentActiveRoute.paramMap.subscribe((result)=>{
      let book_id = result.get("No");
       
      let resultStatus =  this.service.GetBookBookId(book_id);

      resultStatus.subscribe((result)=>{
        console.log(result);
        bookData = result;
        this.book = bookData[0];
      })
    });
  }
  UpdateBook(){
    console.log("Updating Employee " + this.book);  //This is updated employee now!


    let updateStatus =  this.service.UpdateBook(this.book);
    updateStatus.subscribe((result)=>{
      console.log(result);
      this.router.navigate(['/home'])
    });

  }


}
