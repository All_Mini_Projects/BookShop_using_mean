import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  allBooks:any;
  constructor(private service: DataService) { }

  ngOnInit(): void {
    let notify = this.service.GetBooks();

    notify.subscribe((result)=>{
      this.allBooks = result;
    });
  }
}
