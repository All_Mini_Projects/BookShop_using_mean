import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  sellerDetail = {email : "", password : ""};
  message =""

  constructor( private router: Router,private service: DataService) { }

  ngOnInit(): void {
  }
  LogIn(){
    var seller:any;
    this.service.GetSeller(this.sellerDetail.email)
                  .subscribe((response)=>{
                    seller = response;
                    if (seller != null && seller[0].email == this.sellerDetail.email && seller[0].password == this.sellerDetail.password) {
                        console.log("is");
                        this.router.navigate(['home']);
                      }  
                      else
                      {
                        this.message = "Username or Password is wrong!"
                      }       
    });
  }

}
