import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(public http: HttpClient) { }

  GetSeller(Email:any){
    return this.http.get("http://localhost:5000/seller/"+Email);
  }

  AddSeller(sellerToAddedInDatabase:any){
    return this.http.post("http://localhost:5000/seller",sellerToAddedInDatabase);
  }

  GetBooks(){
    return this.http.get("http://localhost:5000/books");
  }

  DeleteBook(book_id:any){
  return this.http.delete("http://localhost:5000/books/"+book_id);
  }
  
  GetBookBookId(book_id:any){
    return this.http.get("http://localhost:5000/books/"+book_id);
  }

  UpdateBook(bookDetails:any){
    return this.http.put("http://localhost:5000/books/"+bookDetails.book_id,bookDetails);
  }

  AddBook(bookData:any){
    return this.http.post("http://localhost:5000/books",bookData);
  }
}
